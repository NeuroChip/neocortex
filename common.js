exports.listen_addr = '0.0.0.0';

exports.credentials = require('./profiles/'+process.env.PERSONNA+'/trousseau/credentials.json');

var chip = require('./profiles/'+process.env.PERSONNA+'/neurochip/config.json');

exports.anatomy = chip.anatomy;
exports.profile = chip.profile;

exports.profile.language = (exports.profile.language || process.env.LANGUAGE) || 'en-US';

/*
exports.ssl = {
    key:  fs.readFileSync('private.key'),
    cert: fs.readFileSync('server.crt'),
};
//*/

exports.cortex = {};
exports.plugin = {};

var autobahn = require('autobahn');

exports.endpoint = {
    wamp: {
        url: exports.credentials.wamp.target,
        realm: exports.credentials.wamp.realm,

        authmethods: ["ticket"],
        authid:      exports.credentials.wamp.pseudo,
        onchallenge: function (session, method, extra) {
            // console.log("onchallenge", method, extra);

            if (method === "ticket") {
               return exports.credentials.wamp.ticket;
            } else {
               throw "don't know how to authenticate using '" + method + "'";
            }
        },
    },
};

exports.target = function (prefix, suffix) {
    var resp = exports.realm + '.' + exports.ident;

    if (exports.verse!='default') {
        resp += '.' + exports.verse;
    }

    while (resp.indexOf('/')!=-1) {
        resp = resp.replace('/', '.');
    }

    return 'reactor.'+prefix+'.'+resp+'.'+suffix;
};

exports.resolve = function (expr) {
    while (expr.indexOf('<self>.')!=-1) {
        expr = expr.replace('<self>.', exports.target('psy',''));
    }

    while (expr.indexOf('<sense>.')!=-1) {
        expr = expr.replace('<sense>.', exports.target('gen',''));
    }

    while (expr.indexOf('<organ>.')!=-1) {
        expr = expr.replace('<organ>.', exports.target('bio',''));
    }

    return expr;
};

exports.invoke = function (method, args) {
    return exports.session.call(exports.resolve(method), args);
};

exports.publish = function (topic, args) {
    return exports.session.publish(exports.resolve(topic), args);
};

exports.diffuse = function (callback) {
    var i, mod;

    for (i=0 ; i<exports.anatomy.cortex.length ; i++) {
        key = exports.anatomy.cortex[i];

        mod = exports.cortex[key];

        callback('cortex', key, mod);
    }

    for (i=0 ; i<exports.anatomy.plugin.length ; i++) {
        key = exports.anatomy.plugin[i];

        mod = exports.plugin[key];

        callback('plugin', key, mod);
    }

    callback(exports.realm, exports.ident, exports[exports.realm]);
};

exports.trigger = function (alias, callback) {
    exports.diffuse(function (type, name, module) {
        if (module.events) {
            handler = module.events[alias];

            if (handler!=null) {
                callback(type, name, handler);
            }
        }
    });
};

exports.patch_mod = function (mod) {
    mod.config = exports;
    mod.creds  = exports.credentials;
    mod.perso  = exports.personal;

    mod.invoke  = exports.invoke;
    mod.publish = exports.publish;

    return mod;
};

exports.kernel = function (realm, identifier, version, callback) {
    exports.realm  = realm;
    exports.ident  = identifier;
    exports.verse  = version || 'default';

    var key,mod;

    console.log("*) Loading implant :");

    for (var i=0 ; i<exports.anatomy.cortex.length ; i++) {
        key = exports.anatomy.cortex[i];

        console.log("\t-> Cortex : ", key);

        mod = require('./nerves/cortex/'+key+'.js');

        exports.cortex[key] = exports.patch_mod(mod);
    }

    for (var i=0 ; i<exports.anatomy.plugin.length ; i++) {
        key = exports.anatomy.plugin[i];

        console.log("\t-> Plugin : ", key);

        mod = require('./nerves/plugin/'+key+'.js');

        exports.plugin[key] = exports.patch_mod(mod);
    }

    mod = require('./organs/'+realm+'/'+identifier+'.js');

    exports[realm] = exports.patch_mod(mod);

    console.log("*) Initializing implant :");

    exports.trigger('init', function (type, name, handler) {
        console.log("\t-> "+type+" : ", name);

        handler(type, name, handler);
    });

    if (callback!=null) {
        callback();
    }
};

exports.program = function (callback) {
    var connection = new autobahn.Connection(exports.endpoint.wamp);

    connection.onopen = function (session) {
        exports.session = session;
        
        console.log("#) Connected implant to : ", session._socket.info.url);

        exports.trigger('connect', function (type, name, handler) {
            handler(session);
        });

        exports.diffuse(function (type, name, module) {
            var hnd, pth;

            Object.keys(module.methods).forEach(function(key) {
                hnd = module.methods[key];

                pth = exports.resolve(key);

                console.log("\t-> Registered procedure '"+pth+"' (at "+key+")");

                session.register(pth, hnd).then(function (res) {
                    /*
                    console.log("\t-> ["+type+"] ("+name+") "+key+" : ");

                    if (type!='cortex') {
                        console.log("\t-> Registered procedure '"+key+"' at : ", pth);
                    }
                    //*/
                });
            });

            Object.keys(module.topics).forEach(function(key) {
                hnd = module.topics[key];

                pth = exports.resolve(key);

                console.log("\t=> Subscribed to topic '"+pth+"' (at "+key+")");

                session.subscribe(pth, hnd).then(function (res) {
                    /*
                    console.log("\t=> ["+type+"] ("+name+") "+key+" : ");

                    if (type!='cortex') {
                        console.log("\t=> Subscribed to topic '"+key+"' at : ", pth);
                    }
                    //*/
                });
            });
        });

        if (callback!=null) {
            callback(session);
        }
    };

    connection.onclose = function (reason, details) {
        exports.trigger('close', function (type, name, handler) {
            handler(reason, details);
        });

        console.log("#) Connection lost : ", reason, details);
    }

    connection.open();
};

