var rdfstore = require('rdfstore');

exports.selector = [
    { s: 'http://example/book', p: null, o: null, g: null },
];

exports.pipeline = function (event, triples) {
    if (event === 'added') {
        exports.publish('reactor.data.rdf.added', [triples]);
    } else if(event === 'deleted') {
        exports.publish('reactor.data.rdf.deleted', [triples]);
    }
};

exports.events = {
    init: function (config) {
        rdfstore.create(function(err, store) {
            for (var i=0 ; i<exports.selector.length ; i++) {
                var r = exports.selector[i];

                store.subscribe(r.s, r.p, r.o, r.g, exports.pipeline);
            }

            exports.store = store;
        });
    },
    open: function (session) {
        
    },
    close: function (reason, details) {
        exports.store.unsubscribe(exports.pipeline);
    },
};

exports.methods = {
    '<self>.rdf.watch': function (args) {
        // exports.store.subscribe("http://example/book",null,null,null,exports.pipeline);
    },
    '<self>.rdf.load': function (args) {
        store.execute('LOAD '+args[1]+' INTO GRAPH '+args[0], function(err) {
            exports.publish('<self>.rdf.import', [args, err]);

            return [args[0], args[1], err];
        });
    },
    '<self>.rdf.sparql': function (args) {
        var stmt = "";

        for (var i=0 ; i<args[1].length ; i++) {
            prefix = args[1][i];

            stmt = stmt+"PREFIX "+prefix.name+":<"+prefix.path+">\n";
        }

        stmt += "\n" + args[0];

        store.execute(stmt, function(err, results) {
            exports.publish('<self>.rdf.sparql', [args, stmt, err, results.length]);

            return results;
        });
    },
};

exports.topics = {
    '<self>.rdf.added': function (triples) {
        console.log(triples.length+" triples have been added");
    },
    '<self>.rdf.deleted': function (triples) {
        console.log(triples.length+" triples have been deleted");
    },
};

exports.program = function (session) {
    exports.invoke('<self>.rdf.load', ['<lisp>', '<http://dbpedialite.org/titles/Lisp_%28programming_language%29>']).then(function (res) {
        console.log("Result:", res);
    });

    exports.invoke('<self>.rdf.sparql', [
        'SELECT ?o FROM NAMED <lisp> { GRAPH <lisp> { ?s foaf:page ?o} }',
        [
            { name: 'foaf', path: "http://xmlns.com/foaf/0.1/" },
        ],
    ]).then(function (res) {
        console.log("Result:", res);
    });
};

