/*
The parietal lobe is one of the four major lobes of the cerebral cortex in the 
brain of mammals. The parietal lobe is positioned above the occipital lobe and 
behind the frontal lobe and central sulcus.

The parietal lobe integrates sensory information among various modalities, 
including spatial sense and navigation (proprioception), the main sensory 
receptive area for the sense of touch (mechanoreception) in the somatosensory 
cortex which is just posterior to the central sulcus in the postcentral gyrus, 
and the dorsal stream of the visual system. The major sensory inputs from the 
skin (touch, temperature, and pain receptors), relay through the thalamus to 
the parietal lobe.

The parietal lobe plays important roles in integrating sensory information from 
various parts of the body, knowledge of numbers and their relations,[3] and in 
the manipulation of objects. Its function also includes processing information 
relating to the sense of touch.[4] Portions of the parietal lobe are involved 
with visuospatial processing. Although multisensory in nature, the posterior 
parietal cortex is often referred to by vision scientists as the dorsal stream 
of vision (as opposed to the ventral stream in the temporal lobe). This dorsal 
stream has been called both the "where" stream (as in spatial vision)[5] and 
the "how" stream (as in vision for action).[6] The posterior parietal cortex 
(PPC) receives somatosensory and/or visual input, which then, through motor 
signals, controls movement of the arm, hand, as well as eye movements.[7]

Various studies in the 1990s found that different regions of the posterior 
parietal cortex in macaques represent different parts of space.
*/

