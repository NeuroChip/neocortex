var Feel = require('multi-sentiment');

var FS = require('fs');

var Manager = module.exports = function Manager(cfg) {
    if (!(this instanceof Manager))
        return new Manager(cfg);

    var self = this;

    self.config = cfg;

    try {
        self.LOUDS = FS.readFileSync(__dirname+'/../data/starters.txt', 'UTF8').trim().split('\n');
    } catch (ERRRRROR) {
        self.LOUDS = [];
    }

    self.apis = {};
};

Manager.prototype.connect = function (name, config, creds, vault) {
    var self = this;

    if (self.apis[name]==null) {
        var handler = require('../networks/'+name);

        self.apis[name] = new handler(self, name, config, creds, vault);
    }

    return self.apis[name];
};

Manager.prototype.mainloop = function (mode, targets) {
    var self = this;

    switch (mode) {
        case 'web':
            break;
        case 'bot':
            for (var i=0 ; i<targets.length ; i++) {
                var api = self.apis[targets[i]];

                api.prepare();

                api.lunch();
            }
            break;
        case 'job':
            break;
    }
};

