var
    CONFIG   = require('./core/config'),
    SLACKBOT = require('./reactor/bot'),
    RESTIFY  = require('restify')
;

var TOKENS;
if (process.env.TOKENS)
{
    TOKENS = process.env.TOKENS.split(',');
}

var LOUDBOT = new SLACKBOT();
var SERVER = RESTIFY.createServer();

SERVER.use(RESTIFY.acceptParser(SERVER.acceptable));
SERVER.use(RESTIFY.queryParser());
SERVER.use(RESTIFY.gzipResponse());
SERVER.use(RESTIFY.bodyParser({ mapParams: false }));

SERVER.get('/PING', function (REQUEST, RESPONSE, NEXT) {
    RESPONSE.send(200, 'PONG');
    NEXT();
});

SERVER.get('/LOUDS', function (REQUEST, RESPONSE, NEXT) {
    RESPONSE.send(LOUDBOT.THELOUDS());
    NEXT();
});

SERVER.post('/MESSAGE', function (REQUEST, RESPONSE, NEXT) {
    if (TOKENS) {
        var IDX = TOKENS.indexOf(REQUEST.body.token);
        if (IDX === -1) return NEXT(new RESTIFY.ForbiddenError('NO LOUDS FOR YOU'));
    }

    var WHAT;
    if (REQUEST.body.user_name !== 'loki')
        WHAT = LOUDBOT.LISTENUP(REQUEST.body);

    if (WHAT)
        RESPONSE.json(200, { text: WHAT, channel: REQUEST.body.channel_name });
    else
        RESPONSE.send(200);

    NEXT();
});

SERVER.listen(process.env.PORT || 4444);

